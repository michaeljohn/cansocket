// Package cansocket implements a CAN socket on Linux.
package cansocket

import (
	"errors"
	"net"

	"golang.org/x/sys/unix"
)

// RawCanSocket represents a CAN socket on Linux.
type RawCanSocket struct {
	fd   int
	name string
}

// NewRawCanSocket create a new CAN socket bound to the given interface name.
func NewRawCanSocket(ifname string) (*RawCanSocket, error) {
	s, err := unix.Socket(unix.AF_CAN, unix.SOCK_RAW, unix.CAN_RAW)
	if err != nil {
		return nil, err
	}
	iface, err := net.InterfaceByName(ifname)
	if err != nil {
		return nil, errors.New("Error getting CAN interface by name: " + err.Error())
	}
	addr := &unix.SockaddrCAN{Ifindex: iface.Index}
	if err := unix.Bind(s, addr); err != nil {
		return nil, err
	}

	return &RawCanSocket{fd: s, name: ifname}, nil
}

// Fd return the file descriptor for the socket.
func (s *RawCanSocket) Fd() int {
	return s.fd
}

// Read a frame off the socket.
func (s *RawCanSocket) Read() (CanFrame, error) {
	return readCanFrame(s.fd)
}

// ReadTimeout reads a frame off the socket and allows a timeout for the operation.
func (s *RawCanSocket) ReadTimeout(timeout *unix.Timeval) (CanFrame, error) {
	var set unix.FdSet
	set.Zero()
	set.Set(s.fd)
	sel, err := unix.Select(s.fd+1, &set, nil, nil, timeout)
	if err != nil {
		return CanFrame{}, err
	}

	if sel != 0 {
		return readCanFrame(s.fd)
	}

	return CanFrame{}, &ErrReadTimeout{}
}

// WriteBytes a frame to the socket.
func (s *RawCanSocket) WriteBytes(data [16]byte) error {
	return writeCanFrame(s.fd, data)
}

// Write frame to the socket.
func (s *RawCanSocket) Write(frame CanFrame) error {
	ba := convertToByteArray(frame)
	return writeCanFrame(s.fd, ba)
}

// WriteFrames writes array of frames to the socket.
func (s *RawCanSocket) WriteFrames(frames []CanFrame) error {
	for _, frame := range frames {
		if err := s.Write(frame); err != nil {
			return err
		}
	}

	return nil
}

// Close the CAN socket.
func (s *RawCanSocket) Close() error {
	return canClose(s.fd)
}

// ---- Custom Errors ----------------

type ErrReadTimeout struct {}

func (e *ErrReadTimeout) Error() string { return "Read timeout reached" }
func (e *ErrReadTimeout) Is(t error) bool {
	if _, ok := t.(*ErrReadTimeout); ok {
		return true
	}
	return false
}
