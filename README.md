# Linux CAN Socket in Go

[![ISC License](https://img.shields.io/badge/license-ISC-blue.svg)](https://gitlab.com/michaeljohn/cansocket/blob/master/LICENSE)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/michaeljohn/cansocket)](https://goreportcard.com/report/gitlab.com/michaeljohn/cansocket)
[![GoDoc](https://godoc.org/gitlab.com/michaeljohn/cansocket?status.svg)](https://godoc.org/gitlab.com/michaeljohn/cansocket)
[![Rate this package](https://badges.openbase.com/go/rating/gitlab.com/michaeljohn/cansocket.svg?token=bAtJGeA8ZdMNYQ0ReC5zF+c0gpuDjCCOc7P1ivX5DVw=)](https://openbase.com/go/gitlab.com/michaeljohn/cansocket?utm_source=embedded&amp;utm_medium=badge&amp;utm_campaign=rate-badge)

## To run Test

The test expects a vcan0 interface to exist. To set this up:

```
modprobe vcan
unshare -n -r --mount <<EOF
ip l add dev vcan0 type can
ip l set up vcan0
go test -v -cover
EOF
```

Or you can run the provided `test.sh` script.

