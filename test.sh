#!/bin/sh

if [ $(lsmod | grep vcan | wc -l) -eq 0 ]
then
	sudo modprobe vcan
fi


unshare -n -r --mount <<EOF
ip link add dev vcan0 type vcan
ip link set up vcan0
go test -v -cover -coverprofile cover.out
EOF
