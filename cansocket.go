// Package cansocket implements a CAN socket on Linux.

//go:build linux
// +build linux

package cansocket

import (
	"encoding/binary"

	"errors"

	"golang.org/x/sys/unix"
)

// From linux-kernel/include/uapi/linux/can.h
//
// CAN frame is always 16 bytes on linux,
// regardless if data payload is 0 byte or 8 bytes.
//   Bytes 0-3   : CAN ID
//   Byte  4     : Frame Payload Length in bytes
//   Bytes 5-7   : Reserved
//   Bytes 8-15  : Frame Data
//
// Controller Area Network Identifier structure
//   bit 0-28  : CAN identifier (11/29 bit)
//   bit 29    : error message frame flag (0 = data frame, 1 = error message)
//   bit 30    : remote transmission request flag (RTR) (0 = data frame, 1 = rtr frame)
//   bit 31    : frame format flag, identifier extension bit (IDE) (0 = standard 11 bit, 1 = extended 29 bit)

// CAN identifier extension bit (IDE)
const (
	IDE_BIT_STANDARD byte = 0
	IDE_BIT_EXTENDED byte = 1
)

// CAN remote transmission request (RTR)
const (
	RTR_BIT_DATA   byte = 0
	RTR_BIT_REMOTE byte = 1
)

// CAN error message
const (
	ERR_FRAME_BIT_DATA  byte = 0
	ERR_FRAME_BIT_ERROR byte = 1
)

// Offset within the CAN ID.
const (
	IDE_BIT_OFFSET uint32 = 31
	RTR_BIT_OFFSET uint32 = 30
	ERR_BIT_OFFSET uint32 = 29
)

// controller area network (CAN) kernel definitions
// valid bits in CAN ID for frame formats
const canStandardMask uint32 = 0x000007FF // standard frame format (SFF)
const canExtendedMask uint32 = 0x1FFFFFFF // extended frame format (EFF)
// special address description flags for the CAN_ID
const canEffFlag uint32 = 0x80000000 // EFF/SFF is set in the MSB
const canRtrFlag uint32 = 0x40000000 // remote transmission request
const canErrFlag uint32 = 0x20000000 // error message frame

// CanFrame represents a CAN bus frame.
type CanFrame struct {
	CanID uint32
	DLC   byte
	IDE   byte
	RTR   byte
	ERR   byte
	Data  [8]byte
}

// Takes the 16-byte array read off the socket and converts it to a CanFrame structure.
func convertToCanFrame(raw [16]byte) (cand CanFrame) {
	// Check if this is a standard or extended frame
	if ((binary.LittleEndian.Uint32(raw[0:4]) & canEffFlag) >> IDE_BIT_OFFSET) == uint32(IDE_BIT_EXTENDED) {
		cand.IDE = IDE_BIT_EXTENDED
		cand.CanID = (binary.LittleEndian.Uint32(raw[0:4]) & canExtendedMask)
	} else {
		cand.IDE = IDE_BIT_STANDARD
		cand.CanID = (binary.LittleEndian.Uint32(raw[0:4]) & canStandardMask)
	}

	cand.RTR = byte((binary.LittleEndian.Uint32(raw[0:4]) & canRtrFlag) >> RTR_BIT_OFFSET)
	cand.ERR = byte((binary.LittleEndian.Uint32(raw[0:4]) & canErrFlag) >> ERR_BIT_OFFSET)
	cand.DLC = raw[4]
	copy(cand.Data[:], raw[8:])
	return
}

// Takes the CanFrame and converts to a 16-byte array.
func convertToByteArray(cframe CanFrame) (bframe [16]byte) {
	id := cframe.CanID |
		(uint32(cframe.IDE) << IDE_BIT_OFFSET) |
		(uint32(cframe.RTR) << RTR_BIT_OFFSET) |
		(uint32(cframe.ERR) << ERR_BIT_OFFSET)
	binary.LittleEndian.PutUint32(bframe[0:4], id)
	bframe[4] = cframe.DLC
	copy(bframe[8:], cframe.Data[:])
	return
}

// Reads a single CAN frame off the socket.
func readCanFrame(fd int) (cand CanFrame, err error) {
	var frame [16]byte
	if _, err = unix.Read(fd, frame[:]); err != nil {
		return cand, err
	}
	return convertToCanFrame(frame), nil
}

// Writes a singe CAN frame to the socket.
func writeCanFrame(fd int, data [16]byte) (err error) {
	var n int
	if n, err = unix.Write(fd, data[:]); err != nil {
		return err
	}
	if n != len(data) {
		return errors.New("Failed to write data on socket")
	}
	return nil
}

// Close the socket.
func canClose(fd int) error {
	return unix.Close(fd)
}
