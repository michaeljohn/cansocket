package cansocket

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"os"
	"testing"
	"time"

	"golang.org/x/sys/unix"
)

// This is a simple test to verify reads and writes are working as expected.

// The local test CAN socket
const CanSocketName = "vcan0"

// Channel of frames read from the bus
var readchan chan CanFrame

// Channel of frames to write on the bus
var writechan chan CanFrame

func startReader() {
	sock, err := NewRawCanSocket(CanSocketName)
	if err != nil {
		fmt.Printf("LISTENER: Failed to open socket: %v\n", err)
		os.Exit(1)
	}

	for {
		frame, err := sock.Read() // TODO add timeout, send failed message on channel
		if err != nil {
			fmt.Printf("LISTENER: Failed to read frame: %v\n", err)
			sock.Close()
			os.Exit(1)
		}
		// fmt.Printf("LISTENER: FRAME: %+v\n", frame)
		readchan <- frame
	}
}

func startWriter() {
	sock, err := NewRawCanSocket(CanSocketName)
	if err != nil {
		fmt.Printf("WRITER: Failed to open socket: %v\n", err)
		os.Exit(1)
	}

	for {
		sock.Write(<-writechan)
	}
}

func TestMain(m *testing.M) {
	readchan = make(chan CanFrame, 2)
	writechan = make(chan CanFrame, 2)
	go startReader()
	go startWriter()

	// Give time for coroutines to start
	time.Sleep(time.Millisecond * 300)

	// Run tests
	ret := m.Run()
	os.Exit(ret)
}

func TestRawCanNew(t *testing.T) {
	sock, err := NewRawCanSocket(CanSocketName)
	if err != nil {
		t.Fatalf("Failed to create new socket")
	}
	sock.Close()
}

func TestFd(t *testing.T) {
	sock, err := NewRawCanSocket(CanSocketName)
	if err != nil {
		t.Fatalf("Failed to create new socket")
	}
	if sock.Fd() < 1 {
		t.Fatalf("Unexpected Fd")
	}
	sock.Close()
}

func TestBadInterfaceName(t *testing.T) {
	_, err := NewRawCanSocket("FooFake")
	if err == nil {
		t.Fatalf("Expected failure on bad CAN interface name")
	}
}

func TestWrite(t *testing.T) {
	sock, err := NewRawCanSocket(CanSocketName)
	if err != nil {
		t.Fatalf("Failed to open socket: %v", err)
	}

	// Create and write a byte array representing a standard CAN frame
	var frame [16]byte
	binary.LittleEndian.PutUint32(frame[0:4], 0x3)
	frame[4] = byte(1)
	frame[8] = 0x88
	if err = sock.WriteBytes(frame); err != nil {
		t.Fatalf("Failed to write frame: %v", err)
	}
	// Wait for frame from the read channel
	f := <-readchan
	// See if what was read is what we wrote
	if f.CanID != binary.LittleEndian.Uint32(frame[0:4]) {
		t.Fatalf("Frame ID doesn't match what was sent")
	}
	if f.DLC != frame[4] {
		t.Fatalf("Frame DLC doesn't match what was sent")
	}
	if 0 != bytes.Compare(f.Data[:], frame[8:]) {
		t.Fatalf("Frame Data doesn't match what was sent")
	}

	// Create a CanFrame representing an extended CAN frame
	cframe := CanFrame{CanID: 0x00459A11, DLC: 4, IDE: 1, Data: [8]byte{0xAA, 0xBB, 0xCC, 0xDD}}
	if err = sock.Write(cframe); err != nil {
		t.Fatalf("Failed to write frame: %v", err)
	}
	// Wait for frame from the read channel
	f = <-readchan
	// See if what was read is what we wrote
	if f.CanID != cframe.CanID {
		t.Fatalf("Frame ID doesn't match what was sent")
	}
	if f.DLC != cframe.DLC {
		t.Fatalf("Frame DLC doesn't match what was sent")
	}
	if f.IDE != cframe.IDE {
		t.Fatalf("Frame IDE doesn't match what was sent")
	}
	if 0 != bytes.Compare(f.Data[:], cframe.Data[:]) {
		t.Fatalf("Frame Data doesn't match what was sent")
	}
	if err = sock.Close(); err != nil {
		t.Fatalf("Failed to close socket: %v", err)
	}
}

func TestWriteFrame(t *testing.T) {
	sock, err := NewRawCanSocket(CanSocketName)
	if err != nil {
		t.Fatalf("Failed to open socket: %v", err)
	}

	// Create an array of CanFrame's representing an extended CAN frame
	var frames [3]CanFrame
	frames[0] = CanFrame{CanID: 0x00459A11, DLC: 4, IDE: 1, Data: [8]byte{0xAA, 0xBB, 0xCC, 0xDD}}
	frames[1] = CanFrame{CanID: 0x00459A12, DLC: 5, IDE: 1, Data: [8]byte{0xAA, 0xBB, 0xCC, 0xDD, 0xEE}}
	frames[2] = CanFrame{CanID: 0x00459A13, DLC: 6, IDE: 1, Data: [8]byte{0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF}}

	if err = sock.WriteFrames(frames[:]); err != nil {
		t.Fatalf("Failed to write frame: %v", err)
	}
	// Read first frame
	f := <-readchan
	// See if what was read is what we wrote
	if f.CanID != frames[0].CanID {
		t.Fatalf("Frame ID doesn't match what was sent")
	}
	if f.DLC != frames[0].DLC {
		t.Fatalf("Frame DLC doesn't match what was sent")
	}
	if !bytes.Equal(f.Data[:], frames[0].Data[:]) {
		t.Fatalf("Frame Data doesn't match what was sent")
	}
	// Read second frame
	f = <-readchan
	// See if what was read is what we wrote
	if f.CanID != frames[1].CanID {
		t.Fatalf("Frame ID doesn't match what was sent")
	}
	if f.DLC != frames[1].DLC {
		t.Fatalf("Frame DLC doesn't match what was sent")
	}
	if !bytes.Equal(f.Data[:], frames[1].Data[:]) {
		t.Fatalf("Frame Data doesn't match what was sent")
	}
	// Read third frame
	f = <-readchan
	// See if what was read is what we wrote
	if f.CanID != frames[2].CanID {
		t.Fatalf("Frame ID doesn't match what was sent")
	}
	if f.DLC != frames[2].DLC {
		t.Fatalf("Frame DLC doesn't match what was sent")
	}
	if !bytes.Equal(f.Data[:], frames[2].Data[:]) {
		t.Fatalf("Frame Data doesn't match what was sent")
	}
	// Close socket
	if err = sock.Close(); err != nil {
		t.Fatalf("Failed to close socket: %v", err)
	}
}

func TestRead(t *testing.T) {
	sock, err := NewRawCanSocket(CanSocketName)
	if err != nil {
		t.Fatalf("Failed to open socket: %v", err)
	}

	// Create a CAN frame that we'll read back
	cframe := CanFrame{CanID: 0x00459A11, DLC: 4, IDE: 1, Data: [8]byte{0xAA, 0xBB, 0xCC, 0xDD}}
	writechan <- cframe

	// Read a CAN frame
	rframe, err := sock.Read()
	if err != nil {
		t.Fatalf("Failed to read frame: %v", err)
	}
	// See if what we got is what we expect
	if rframe.CanID != cframe.CanID {
		t.Fatalf("Frame ID doesn't match what was sent")
	}
	if rframe.DLC != cframe.DLC {
		t.Fatalf("Frame DLC doesn't match what was sent")
	}
	if rframe.IDE != cframe.IDE {
		t.Fatalf("Frame IDE doesn't match what was sent")
	}
	if 0 != bytes.Compare(rframe.Data[:], cframe.Data[:]) {
		t.Fatalf("Frame Data doesn't match what was sent")
	}
	if err = sock.Close(); err != nil {
		t.Fatalf("Failed to close socket: %v", err)
	}
}

func TestReadTimeout(t *testing.T) {
	sock, err := NewRawCanSocket(CanSocketName)
	if err != nil {
		t.Fatalf("Failed to open socket: %v", err)
	}

	// Create a CAN frame that we'll read back
	cframe := CanFrame{CanID: 0x00459A11, DLC: 4, IDE: 1, Data: [8]byte{0xAA, 0xBB, 0xCC, 0xDD}}
	writechan <- cframe

	// Read a CAN frame
	rframe, err := sock.ReadTimeout(&unix.Timeval{Sec: 2})
	if err != nil {
		t.Fatalf("Failed to read frame: %v", err)
	}
	// See if what we got is what we expect
	if rframe.CanID != cframe.CanID {
		t.Fatalf("Frame ID doesn't match what was sent")
	}
	if rframe.DLC != cframe.DLC {
		t.Fatalf("Frame DLC doesn't match what was sent")
	}
	if rframe.IDE != cframe.IDE {
		t.Fatalf("Frame IDE doesn't match what was sent")
	}
	if 0 != bytes.Compare(rframe.Data[:], cframe.Data[:]) {
		t.Fatalf("Frame Data doesn't match what was sent")
	}
	if err = sock.Close(); err != nil {
		t.Fatalf("Failed to close socket: %v", err)
	}
}

func TestReadTimeout_Timeout(t *testing.T) {
	sock, err := NewRawCanSocket(CanSocketName)
	if err != nil {
		t.Fatalf("Failed to open socket: %v", err)
	}

	// Create a CAN frame that we'll read back
	// cframe := CanFrame{CanID: 0x00459A11, DLC: 4, IDE: 1, Data: [8]byte{0xAA, 0xBB, 0xCC, 0xDD}}
	// writechan <- cframe

	// Read a CAN frame
	_, err = sock.ReadTimeout(&unix.Timeval{Sec: 2})
	if err == nil {
		t.Fatalf("Expected an error to be returned")
	}
	if !errors.Is(err, &ErrReadTimeout{}) {
		t.Fatalf("Expected timeout error")
	}
}

func TestConvertToByteArrayStandard(t *testing.T) {
	cframe := CanFrame{CanID: 0x00000711, DLC: 4, IDE: IDE_BIT_STANDARD, Data: [8]byte{0xAA, 0xBB, 0xCC, 0xDD}}
	ba := convertToByteArray(cframe)
	if ((binary.LittleEndian.Uint32(ba[0:4]) & canEffFlag) >> IDE_BIT_OFFSET) != uint32(IDE_BIT_STANDARD) {
		t.Fatalf("Frame IDE doesn't match")
	}
	if (binary.LittleEndian.Uint32(ba[0:4]) & canStandardMask) != cframe.CanID {
		t.Fatalf("Frame CANID doesn't match")
	}
	if ba[4] != cframe.DLC {
		t.Fatalf("Frame DLC doesn't match")
	}
	if ((binary.LittleEndian.Uint32(ba[0:4]) & canRtrFlag) >> RTR_BIT_OFFSET) != uint32(RTR_BIT_DATA) {
		t.Fatalf("Frame RTR doesn't match")
	}
}

func TestConvertToByteArrayExtended(t *testing.T) {
	cframe := CanFrame{CanID: 0x00459A11, DLC: 4, IDE: IDE_BIT_EXTENDED, Data: [8]byte{0xAA, 0xBB, 0xCC, 0xDD}}
	ba := convertToByteArray(cframe)
	if ((binary.LittleEndian.Uint32(ba[0:4]) & canEffFlag) >> IDE_BIT_OFFSET) != uint32(cframe.IDE) {
		t.Fatalf("Frame IDE doesn't match")
	}
	if (binary.LittleEndian.Uint32(ba[0:4]) & canExtendedMask) != cframe.CanID {
		t.Fatalf("Frame CANID doesn't match")
	}
	if ba[4] != cframe.DLC {
		t.Fatalf("Frame DLC doesn't match")
	}
	if ((binary.LittleEndian.Uint32(ba[0:4]) & canRtrFlag) >> RTR_BIT_OFFSET) != uint32(RTR_BIT_DATA) {
		t.Fatalf("Frame RTR doesn't match")
	}
}

func TestConvertToCanFrameStandard(t *testing.T) {
	var frame [16]byte
	binary.LittleEndian.PutUint32(frame[0:4], 0x3)
	frame[4] = byte(1)
	frame[8] = 0x88
	cf := convertToCanFrame(frame)
	if cf.CanID != uint32(0x3) {
		t.Fatalf("Frame CANID doesn't match")
	}
	if cf.IDE != IDE_BIT_STANDARD {
		t.Fatalf("Frame IDE doesn't match")
	}
	if cf.DLC != byte(1) {
		t.Fatalf("Frame DLC doesn't match")
	}
	if cf.Data[0] != 0x88 {
		t.Fatalf("Frame Data doesn't match")
	}
}

func TestConvertToCanFrameExtended(t *testing.T) {
	id := uint32(0x45A13) | (uint32(IDE_BIT_EXTENDED) << IDE_BIT_OFFSET)
	var frame [16]byte
	binary.LittleEndian.PutUint32(frame[0:4], id)
	frame[4] = byte(1)
	frame[8] = 0x88
	cf := convertToCanFrame(frame)
	if cf.CanID != uint32(0x45A13) {
		t.Fatalf("Frame CANID doesn't match")
	}
	if cf.IDE != IDE_BIT_EXTENDED {
		t.Fatalf("Frame IDE doesn't match")
	}
	if cf.DLC != byte(1) {
		t.Fatalf("Frame DLC doesn't match")
	}
	if cf.Data[0] != 0x88 {
		t.Fatalf("Frame Data doesn't match")
	}
}
